import {connect, subscribeToTicker, unsubscribeFromTicker} from './web-socket'

let ports = [];
let connected = false;

self.addEventListener("connect", e => {
  const port = e.ports[0];

  function sendTicker(symbol, price, isValid) {
    port.postMessage({symbol, price, isValid});
  }

  port.addEventListener("message", event => {
    const {message, symbol} = event.data;

    switch (message) {
      case 'start':
        if (!connected) {
          connected = true;
          connect();
        }
        ports.push(port);
        break;
      case 'stop':
        ports = ports.filter(p => p !== port);
        break;
      case 'subscribe':
        subscribeToTicker(symbol, sendTicker);
        break;
      case 'unsubscribe':
        unsubscribeFromTicker(symbol, sendTicker);
        break;
    }
  });

  port.start();
});