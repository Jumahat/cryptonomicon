const url = new URL(window.location.toString());

function tryToPush() {
  if (history.pushState) {
    history.pushState(null, null, url.toString());
  } else {
    console.warn('History API не поддерживается');
  }
}

export function setToURL(name, value) {
  url.searchParams.set(name, value);
  tryToPush();
}

export function deleteFromURL(name) {
  url.searchParams.delete(name);
  tryToPush();
}

export function getFromURL(name) {
  return url.searchParams.get(name);
}

export function setToLocalStorage(name, value) {
  localStorage.setItem(name, JSON.stringify(value));
}

export function getFromLocalStorage(name) {
  return JSON.parse(localStorage.getItem(name));
}