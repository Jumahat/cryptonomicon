import { createApp } from 'vue'
import App from './App.vue'
import './assets/index.scss'
import './assets/tailwind.css'

createApp(App).mount('#app')