const REST_URL = new URL('https://min-api.cryptocompare.com');
const CURRENCIES_URL = new URL('/data/all/coinlist', REST_URL);
CURRENCIES_URL.searchParams.set('summary', 'true');

export async function getAllCurrencies() {
  try {
    const response = await fetch(CURRENCIES_URL.toString());
    const json = await response.json();
    return Object.values(json['Data']).map(currency => {
      return {
        name: currency.FullName,
        symbol: currency.Symbol,
      }
    });
  } catch (err) {
    console.error(err);
  }
}