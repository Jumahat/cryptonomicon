const API_KEY = 'a31d4c5b104d26a053dbc7a23bd81f4559977d688e156566d64ead90a8055979';
const WS_URL = new URL('/v2', 'wss://streamer.cryptocompare.com');
WS_URL.searchParams.set('api_key', API_KEY);

let socket = null;
let BTCRate = null;

function convertToUSD(price) {
  return BTCRate ? price * BTCRate : '-';
}

function sendWS(action, fromSymbol, toSymbol) {
  const message = {
    action: action,
    subs: [`5~CCCAGG~${fromSymbol}~${toSymbol}`],
  };

  if (socket.readyState === socket.OPEN) {
    socket.send(JSON.stringify(message));
  }

  socket.addEventListener('open', () => {
    socket.send(JSON.stringify(message));
  });
}

function trackTicker(fromSymbol, toSymbol) {
  sendWS('SubAdd', fromSymbol, toSymbol);
}

function untrackTicker(fromSymbol, toSymbol) {
  sendWS('SubRemove', fromSymbol, toSymbol);
}

export function connect() {
  socket = new WebSocket(WS_URL.toString());

  socket.addEventListener('open', () => {
    subscribeToTicker('BTC', (symbol, price) => BTCRate = price);
  });

  socket.addEventListener('message', (event) => {
    const {
      TYPE: type,
      FROMSYMBOL: fromSymbol,
      TOSYMBOL: toSymbol,
      PRICE: price,
      MESSAGE: message,
      PARAMETER: parameter
    } = JSON.parse(event.data);

    switch (type) {
      case '5':
        if (fromSymbol && price) {
          switch (toSymbol) {
            case 'USD':
              if (fromSymbol === 'BTC') {
                BTCRate = price;
              }
              executeHandlers(fromSymbol, price, true);
              break;
            case 'BTC':
              executeHandlers(fromSymbol, convertToUSD(price), true);
              break;
          }
        }
        break;
      case '500':
        if (message === 'INVALID_SUB') {
          const regExp = parameter.match(/^5~CCCAGG~(.*)~(.*)$/);
          if (regExp) {
            const [, parameterFromSymbol, parameterToSymbol] = regExp;
            const ticker = tickers.get(parameterFromSymbol);

            switch (parameterToSymbol) {
              case 'USD':
                ticker.toSymbol = 'BTC';
                trackTicker(parameterFromSymbol, 'BTC');
                break;
              case 'BTC':
                executeHandlers(parameterFromSymbol, '-', false);
                tickers.delete(parameterFromSymbol);
                break;
            }
          }
        }
        break;
    }
  });
}

const tickers = new Map();

function executeHandlers(symbol, price, isValid) {
  const ticker = tickers.get(symbol);
  if (ticker) {
    ticker.handlers.forEach(handler => handler(symbol, price, isValid));
  }
}

export function subscribeToTicker(tickerSymbol, callback) {
  if (!tickers.has(tickerSymbol)) {
    trackTicker(tickerSymbol, 'USD');
    tickers.set(tickerSymbol, {
      toSymbol: 'USD',
      handlers: [callback]
    });
  } else {
    const ticker = tickers.get(tickerSymbol);
    ticker.handlers.push(callback);
  }
}

export function unsubscribeFromTicker(tickerSymbol, callback) {
  if (!tickers.has(tickerSymbol)) {
    return;
  }

  const ticker = tickers.get(tickerSymbol);
  ticker.handlers = ticker.handlers.filter(handler => handler !== callback);

  if (!ticker.handlers.length) {
    untrackTicker(tickerSymbol, ticker.toSymbol);
    tickers.delete(tickerSymbol);
  }
}