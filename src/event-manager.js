const shared = new SharedWorker('./shared-worker.js', {
  type: 'module',
  name: 'web-socket'
});

shared.port.addEventListener('message', event => {
  const {symbol, price, isValid} = event.data;
  executeHandlers(symbol, price, isValid);
});

shared.port.start();
shared.port.postMessage({message: 'start'});

window.addEventListener('beforeunload', function () {
  shared.port.postMessage({message: 'stop'});
});

const tickers = new Map();

function executeHandlers(symbol, price, isValid) {
  const tickerHandlers = tickers.get(symbol);
  tickerHandlers.forEach(handler => handler(symbol, price, isValid));
}

export function subscribeToTicker(tickerSymbol, callback) {
  if (!tickers.has(tickerSymbol)) {
    shared.port.postMessage({message: 'subscribe', symbol: tickerSymbol});
  }

  tickers.set(tickerSymbol, [...(tickers.get(tickerSymbol) || []), callback]);
}

export function unsubscribeFromTicker(tickerSymbol, callback) {
  if (!tickers.has(tickerSymbol)) {
    return;
  }

  const handlers = tickers.get(tickerSymbol).filter(handler => handler !== callback);

  if (handlers.length) {
    tickers.set(tickerSymbol, handlers);
  } else {
    shared.port.postMessage({message: 'unsubscribe', symbol: tickerSymbol});
    tickers.delete(tickerSymbol);
  }
}